# wc-django-chatting

## Installation
pipenv
```shell
pipenv install git+https://gitlab.com/eighthcolor22/wc-django-chatting.git@dev#egg=wc-django-chatting
```
poetry
```shell
poetry add git+https://gitlab.com/eighthcolor22/wc-django-chatting.git#dev
```

## Base local configs:

#### 1. Create server/centrifugo.json and add it to .gitignore
``` json
{
  "token_hmac_secret_key": "CHANGE",
  "admin_password": "admin",
  "admin_secret": "CHANGE",
  "api_key": "CHANGE"
}
```
#### 2. Add to docker-compose file if use docker-compose
```yaml
  centrifugo:
    image: centrifugo/centrifugo:v2.8.5
    container_name: example_centrifugo
    command: centrifugo -c config.json --port=8001  --log_level=debug --debug --admin --address=0.0.0.0
    ports:
      - "8001:8001"
    volumes:
      - ./server/centrifugo.json:/centrifugo/config.json
```
#### 3. Add centrifugo consts to server/.env:

```dotenv
CENTRIFUGO_HOST=http://centrifugo
CENTRIFUGO_API_KEY=CHANGE
CENTRIFUGO_HMAC_KEY=CHANGE
CENTRIFUGO_LOCAL_HOST=http://localhost
```
#### 4. Add next to project settings :
```python
SITE_NAME = 'your_site'
CENTRIFUGO_HOST = env('CENTRIFUGO_HOST', default='http://centrifugo')
CENTRIFUGO_PORT = env('CENTRIFUGO_PORT', default=8001)
CENTRIFUGO_HMAC_KEY = env('CENTRIFUGO_HMAC_KEY')
CENTRIFUGO_API_KEY = env('CENTRIFUGO_API_KEY')
CENTRIFUGO_LOCAL_HOST = env('CENTRIFUGO_LOCAL_HOST', default=None)
```


#### 5. Add next to apps in settings:
```python
PROJECT_APPS = [
    ...
    'wcd_chatting',
    'wcd_chatting.backends.django_orm',
    'wcd_chatting.contrib.centrifugo',
    ...
]
```

#### 6. Add URL-pattern to the urlpatterns of your Django project urls.py file
```python
urlpatterns = patterns(
    ...
    path('', include("wcd_chatting.contrib.centrifugo.urls")),
    ...
)
```
## Base usage:

```python
from django.conf import settings as django_settings

from wcd_chatting.api import APIClient
from wcd_chatting.confs import Settings, ChatEvents, DEFAULTS as CHAT_DEFAULTS
from wcd_chatting.contrib.centrifugo.sender import centrifugo_sender

chat = APIClient(
    settings=Settings(CHAT_DEFAULTS),
    event_classes=ChatEvents(),
    sender=centrifugo_sender,
)

chat.update_settings(
    chat.update_settings(
        {
            'TIME_ZONE': django_settings.TIME_ZONE,
            'ROOMS_RESOLVER_PIPELINE': [
                'wcd_chatting.pipelines.update_rooms_members_profile_data',
            ],
            'MESSAGES_RESOLVER_PIPELINE': [
                'wcd_chatting.pipelines.update_messages_author_profile_data',
            ],
        }
    )
)
```

## REST:
```python
from wcd_chatting.http_api.views import (
    RoomsListAPIView as CRoomsListAPIView,
)
from ..client import chat


class RoomsListAPIView(CRoomsListAPIView):
    api_client = chat
```
